import 'package:beer_stock/Product.dart';
import 'package:flutter/material.dart';

class ProductList extends StatefulWidget {
  @override
  _ProductListState createState() => new _ProductListState();
}

class _ProductListState extends State<ProductList> {
  var _items;

  @override
  Widget build(BuildContext context) {
    return new RefreshIndicator(
      child: ListView(
          physics: const AlwaysScrollableScrollPhysics(), children: _items),
      onRefresh: _handleRefresh,
    );
  }

  List<ProductWidget> _getItems() {
    var items = <ProductWidget>[];
    items.add(ProductWidget(
      ean: "5410228142218",
      quantity: 5,
    ));
    items.add(ProductWidget(ean: "3080216049632", quantity: 0));
    items.add(ProductWidget(ean: "54050082", quantity: 3));
    return items;
  }

  Future<Null> _handleRefresh() async {
//

    setState(() {
      this._items = <ProductWidget>[];
    });

    await new Future.delayed(new Duration(seconds: 1));

    setState(() {
      this._items = _getItems();
    });

    return null;
  }

  @override
  void initState() {
    super.initState();
    _handleRefresh().then(null);
  }
}
